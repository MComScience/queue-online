import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      key: 'vuex-store',
      storage: window.localStorage,
      paths: ['counter', 'i18n']
    })(store)
    store.dispatch('i18n/init')
  })
}
