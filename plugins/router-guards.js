import NProgress from 'nprogress'

export default ({ app }) => {
  app.router.beforeResolve((to, from, next) => {
    if (to.name) {
      NProgress.start()
    }
    next()
  })

  app.router.afterEach(() => {
    NProgress.done()
  })
}
