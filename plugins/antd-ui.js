import Antd from 'ant-design-vue'
import Vue from 'vue'

export default () => {
  Vue.use(Antd)
}
