const pkg = require('./package')
// const webpack = require('webpack')

const isDev = process.env.NODE_ENV === 'development'

module.exports = {
  mode: 'universal',
  // host
  server: {
    port: 8000, // default: 3000
    host: isDev ? 'localhost' : '192.168.1.5' // default: localhost
  },

  /* cli */
  cli: {
    bannerColor: 'gray'
  },

  /* env */
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },

  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'th'
    },
    bodyAttrs: {
      class: [/* 'dark-mode',  */ 'mobile', 'fixed-navbar sidebar-scroll']
    },
    title: pkg.name,
    titleTemplate: '%s | ระบบนัดโรงพยาบาลอุดรธานี',
    meta: [
      { charset: 'utf-8' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Prompt&display=swap'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#b5d56a' },

  /*
   ** Global CSS
   */
  css: [
    'ant-design-vue/dist/antd.css',
    '~/assets/css/transition.css',
    '~/assets/css/nprogress.css',
    '~/assets/css/main.css',
    'bootstrap/dist/css/bootstrap.css',
    'animate.css/animate.min.css',
    'font-awesome/css/font-awesome.min.css',
    '~/assets/css/pe-icon-7-stroke.css',
    '~/assets/css/style.css'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/antd-ui',
    '~/plugins/i18n.js',
    { src: '~/plugins/vuex-persistedstate.js', ssr: false },
    { src: '~/plugins/router-guards.js', ssr: false },
    { src: '~/plugins/highcharts.js', ssr: false }
    // { src: '~/plugins/bootstrap.js', mode: 'client' }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@bazzite/nuxt-optimized-images'
  ],

  optimizedImages: {
    optimizeImages: true
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /* routes */
  router: {
    middleware: 'i18n'
  },
  generate: {
    routes: ['/', '/about', '/th', '/th/about']
  },

  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
    /* plugins: [
      // set shortcuts as global for bootstrap
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ] */
  }
}
